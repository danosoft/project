from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class ContactsHook(CMSApp):
    name = _("Contacts")
    urls = ["contacts.urls"]
    app_name = "contacts"

apphook_pool.register(ContactsHook)

class ContactViewLandingApp(CMSApp):
    name = _("Contact View Landing")
    urls = ['contacts.urls_landing']
    app_name = "contacts_landing"


apphook_pool.register(ContactViewLandingApp)

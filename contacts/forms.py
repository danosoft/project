from django import forms
from django.forms.widgets import SelectDateWidget
from django.utils.html import mark_safe
from contacts.models import Contact, TipoContratti
from django.utils.translation import ugettext_lazy as _

class HorizRadioRenderer(forms.RadioSelect.renderer):
    """ this overrides widget method to put radio buttons horizontally
        instead of vertically.
    """
    def render(self):
            """Outputs radios"""
            return mark_safe(u'\n'.join([u'%s\n' % w for w in self]))

class ContactForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        self.user = self.request.user
        super(ContactForm, self).__init__(*args, **kwargs)
        # self.fields['datainiziolocazione'] = forms.DateTimeField(label="aaaaaaa", required=True)
        self.fields['tiposervizio'] = forms.ChoiceField(widget=forms.Select(attrs={"onChange": 'tiposerviziox(this)'}), choices=Contact.CONTACT_TYPE_SERV)
        self.fields['tipocontratto'] = forms.ModelMultipleChoiceField(required=False, widget=forms.CheckboxSelectMultiple(), queryset=TipoContratti.objects.all())

        self.fields['presgara'] = forms.ChoiceField(widget=forms.RadioSelect, choices=Contact.CONTACT_TYPE_CEDO)
        self.fields['cedosecca'] = forms.ChoiceField(widget=forms.RadioSelect, choices=Contact.CONTACT_TYPE_CEDO)

        self.fields['calcolodovuto'].widget = forms.HiddenInput()
        # self.fields['canonetot'] = forms.CharField(label=_('Canone totale solo dei primi 12 mesi Euro* (come separatore decimale usare il "." '), required=True)

        self.fields['datainiziolocazione'].widget.attrs['readonly'] = True
        self.fields['datastipulacontratto'].widget.attrs['readonly'] = True
        self.fields['dataprimascadenzacontratto'].widget.attrs['readonly'] = True


        self.fields['denoage'].widget.attrs['readonly'] = True
        self.fields['first_name'].widget.attrs['readonly'] = True
        self.fields['email'].widget.attrs['readonly'] = True
        self.fields['city'].widget.attrs['readonly'] = True
        self.fields['tel'].widget.attrs['readonly'] = True
        self.fields['coupon'].widget.attrs['readonly'] = True
        # self.fields['coupon'].widget = forms.HiddenInput()

        self.fields['classe'].widget.attrs['readonly'] = True
        self.fields['classe2'].widget.attrs['readonly'] = True
        self.fields['classe3'].widget.attrs['readonly'] = True
        self.fields['classe4'].widget.attrs['readonly'] = True




        # self.fields['datainiziolocazione'] = forms.DateInput(
        #     widget=SelectDateWidget(
        #         empty_label=("Anno", "Mese", "Giorno"),
        #     ),
        # )
        # self.fields['datastipulacontratto'] = forms.DateField(
        #     widget=SelectDateWidget(
        #         empty_label=("Anno", "Mese", "Giorno"),
        #     ),
        # )
        # self.fields['dataprimascadenzacontratto'] = forms.DateField(
        #     widget=SelectDateWidget(
        #         empty_label=("Anno", "Mese", "Giorno"),
        #     ),
        # )


        # self.fields['tipocontratto'] = forms.CharField(label="Tipologia di Contratto due",
        #                                                widget=forms.RadioSelect(renderer=HorizRadioRenderer,choices=Contact.CONTACT_TYPE_CHOICES), required=True)


    class Meta:
        model = Contact
        fields = ('first_name', 'nome_registrazione',
                  'datastipulacontratto', 'datainiziolocazione', 'dataprimascadenzacontratto',
                  'tiposervizio', 'tipocontr', 'tipocontratto',
                  'nrcopie', 'cedosecca', 'notecedosecca', 'presgara', 'codfisgara',
                  'canonetot', 'email', 'elecodfislocatori', 'message', 'elecodfislocatari', 'city', 'tel', 'contratto', 'visura',
                  'ape', 'allegatigen',
                  'newsletter', 'privacy_ok', 'modalitapagamento', 'calcolodovuto',
                  'classe', 'rendita', 'foglio', 'particella', 'subalterno', 'categoria',
                  'classe2', 'rendita2', 'foglio2', 'particella2', 'subalterno2', 'categoria2',
                  'classe3', 'rendita3', 'foglio3', 'particella3', 'subalterno3', 'categoria3',
                  'classe4', 'rendita4', 'foglio4', 'particella4', 'subalterno4', 'categoria4',
                  'numimsec','numloc', 'perposs','numimsec2','numloc2', 'perposs2',
                  'numimsec3', 'numloc3', 'perposs3','numimsec4','numloc4', 'perposs4',
                  'numimsec5', 'numloc5', 'perposs5','numimsec6','numloc6', 'perposs6',
                  'numimsec7', 'numloc7', 'perposs7','numimsec8','numloc8', 'perposs8',
                  'numimsec9', 'numloc9', 'perposs9','numimsec10','numloc10', 'perposs10',
                  'coupon', 'denoage', )
        exclude = ()

    # def clean(self):
    #     shipping = self.cleaned_data.get('tiposervizio')
    #     if shipping:
    #         self.fields_required(['canonetot','elecodfislocatori', 'elecodfislocatari', ])
    #     else:
    #         self.cleaned_data['shipping_destination'] = ''
    #     return self.cleaned_data
    #
    # def fields_required(self, fields):
    #     """Used for conditionally marking fields as required."""
    #     for field in fields:
    #         if not self.cleaned_data.get(field, ''):
    #             msg = forms.ValidationError("This field is required.")
    #             self.add_error(field, msg)


    # def clean_tipocontratto(self):
    #     if len(self.cleaned_data['tipocontratto']) > 4:
    #         raise forms.ValidationError('Seleziona non piu di 3 elementiSelect no more than 3.')
    #     return self.cleaned_data['tipocontratto']

    def save(self, commit=True):
        instance = super(ContactForm, self).save(commit=False)

        # Prepare a 'save_m2m' method for the form,
        # old_save_m2m = self.save_m2m
        # def save_m2m():
        #     old_save_m2m()
        #     # This is where we actually link the pizza with toppings
        #     instance.tipocontratto_set.clear()
        #     for tipocontratto in self.cleaned_data['tipocontratto']:
        #         instance.tipocontratto_set.add(tipocontratto)
        #
        # self.save_m2m = save_m2m

        instance.created_by = self.user
        instance.changed_by = self.user
        if commit:
            instance.save()
            self.save_m2m()
        return instance
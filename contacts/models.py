# -*- coding: utf-8 -*-
from django.db import models
from pleiadi_content.content.models import AdministrativeMixin
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User
from django import forms
from django.utils.text import capfirst
from django.core import exceptions
from django.utils.translation import ugettext_lazy as _

class TipoContratti(models.Model):
    titolo = models.CharField(_('Titolo'), max_length=255, blank=True, null=True)
    order = models.IntegerField('order', default=1)

    def __unicode__(self):
        return '%s' % (self.titolo,)

    class Meta:
        verbose_name = _('Tipo Contratto')
        verbose_name_plural = _('Tipo Contratti')
        ordering = ('order', 'titolo', )


class Contact(AdministrativeMixin):
    CONTACT_TYPE_SERV = (
        ('reg', _('REGISTRAZIONE')),
        ('annsucc', _('ANNUALITA\' SUCCESSIVE')),
        ('proro', _('PROROGA')),
        ('cess', _('CESSIONE')),
        ('ris', _('RISOLUZIONE')),
        ('cong', _('CONGUAGLIO D\'IMPOSTA'))
    )

    CONTACT_TYPE_CONTR = (
        ('prv', _('PERSONA FISICA')),
        ('com', _('ALTRI SOGGETTI'))
    )

    CONTACT_TYPE_PAGA = (
        ('bon', _('Bonifico')),
        ('ppc', _('Paypal/Carta di Credito'))
    )

    CONTACT_TYPE_CEDO = (
        ('Si', 'Si'),
        ('No', 'No')
    )

    nome_registrazione = models.CharField(_('Nome servizio richiesto'), max_length=255, blank=False, null=False)
    datastipulacontratto = models.DateTimeField()
    datainiziolocazione = models.DateTimeField()
    dataprimascadenzacontratto = models.DateTimeField()
    tiposervizio = models.CharField(_('Tipo servizio'), max_length=255, blank=False, null=False,choices = CONTACT_TYPE_SERV)
    tipocontr = models.CharField(_('Tipologia richiedente'), max_length=255, blank=False, null=False,choices=CONTACT_TYPE_CONTR)
    # tipocontratto = models.CharField(_('Tipologia contratto'), max_length=255, blank=True, null=True, choices=CONTACT_TYPE_CHOICES)
    tipocontratto = models.ManyToManyField(TipoContratti, related_name='tipocontratti', blank=True,)
    nrcopie = models.IntegerField(_('Num. copie da registrare'), default=1, blank=True, null=True)
    presgara = models.CharField(_('Presenza di garanzie'), max_length=10, blank=True, null=True, choices=CONTACT_TYPE_CEDO)
    codfisgara = models.CharField(_('Codice fiscale garante'), max_length=255, blank=True, null=True)

    canonetot = models.FloatField(_('Canone totale solo dei primi 12 mesi Euro'), blank=False, null=False)
    elecodfislocatori = models.TextField(_('Elenco codici fiscale locatori (uno per riga)'), blank=True, null=True)
    elecodfislocatari = models.TextField(_('Elenco codici fiscale locatari (uno per riga)'), blank=True, null=True)

    #cedolare secca
    cedosecca = models.CharField(_('Cedolare secca'), max_length=10, blank=True, null=True, choices=CONTACT_TYPE_CEDO)
    notecedosecca = models.CharField(_('Note Cedolare secca'), max_length=255, blank=True, null=True)
    numimsec = models.IntegerField(_('Nr. Immobile'), validators=[MaxValueValidator(4), MinValueValidator(1)], blank=True, null=True)
    numloc = models.IntegerField(_('Nr. Locatore'), blank=True, null=True)
    perposs = models.IntegerField(_('% Possesso'), blank=True, null=True)
    numimsec2 = models.IntegerField(_('Nr. Immobile'), validators=[MaxValueValidator(4), MinValueValidator(1)], blank=True, null=True)
    numloc2 = models.IntegerField(_('Nr. Locatore'), blank=True, null=True)
    perposs2 = models.IntegerField(_('% Possesso'), blank=True, null=True)
    numimsec3 = models.IntegerField(_('Nr. Immobile'), validators=[MaxValueValidator(4), MinValueValidator(1)], blank=True,null=True)
    numloc3 = models.IntegerField(_('Nr. Locatore'), blank=True, null=True)
    perposs3 = models.IntegerField(_('% Possesso'), blank=True, null=True)
    numimsec4 = models.IntegerField(_('Nr. Immobile'), validators=[MaxValueValidator(4), MinValueValidator(1)],
                                    blank=True, null=True)
    numloc4 = models.IntegerField(_('Nr. Locatore'), blank=True, null=True)
    perposs4 = models.IntegerField(_('% Possesso'), blank=True, null=True)
    numimsec5 = models.IntegerField(_('Nr. Immobile'), validators=[MaxValueValidator(4), MinValueValidator(1)],
                                    blank=True, null=True)
    numloc5 = models.IntegerField(_('Nr. Locatore'), blank=True, null=True)
    perposs5 = models.IntegerField(_('% Possesso'), blank=True, null=True)
    numimsec6 = models.IntegerField(_('Nr. Immobile'), validators=[MaxValueValidator(4), MinValueValidator(1)],
                                    blank=True, null=True)
    numloc6 = models.IntegerField(_('Nr. Locatore'), blank=True, null=True)
    perposs6 = models.IntegerField(_('% Possesso'), blank=True, null=True)
    numimsec7 = models.IntegerField(_('Nr. Immobile'), validators=[MaxValueValidator(4), MinValueValidator(1)],
                                    blank=True, null=True)
    numloc7 = models.IntegerField(_('Nr. Locatore'), blank=True, null=True)
    perposs7 = models.IntegerField(_('% Possesso'), blank=True, null=True)
    numimsec8 = models.IntegerField(_('Nr. Immobile'), validators=[MaxValueValidator(4), MinValueValidator(1)],
                                    blank=True, null=True)
    numloc8 = models.IntegerField(_('Nr. Locatore'), blank=True, null=True)
    perposs8 = models.IntegerField(_('% Possesso'), blank=True, null=True)
    numimsec9 = models.IntegerField(_('Nr. Immobile'), validators=[MaxValueValidator(4), MinValueValidator(1)],
                                    blank=True, null=True)
    numloc9 = models.IntegerField(_('Nr. Locatore'), blank=True, null=True)
    perposs9 = models.IntegerField(_('% Possesso'), blank=True, null=True)
    numimsec10 = models.IntegerField(_('Nr. Immobile'), validators=[MaxValueValidator(4), MinValueValidator(1)],
                                    blank=True, null=True)
    numloc10 = models.IntegerField(_('Nr. Locatore'), blank=True, null=True)
    perposs10 = models.IntegerField(_('% Possesso'), blank=True, null=True)

    # dati catastali
    classe = models.CharField(_('Nr. Immobile'), max_length=10, blank=True, null=True)
    foglio = models.CharField(_('Foglio'), max_length=10, blank=True, null=True)
    particella = models.CharField(_('Particella'), max_length=10, blank=True, null=True)
    subalterno = models.CharField(_('Subalterno'), max_length=10, blank=True, null=True)
    categoria = models.CharField(_('Categoria'), max_length=10, blank=True, null=True)
    rendita = models.FloatField(_('Rendita'), blank=True, null=True)
    classe2 = models.CharField(_('Nr. Immobile'), max_length=10, blank=True, null=True)
    foglio2 = models.CharField(_('Foglio'), max_length=10, blank=True, null=True)
    particella2 = models.CharField(_('Particella'), max_length=10, blank=True, null=True)
    subalterno2 = models.CharField(_('Subalterno'), max_length=10, blank=True, null=True)
    categoria2 = models.CharField(_('Categoria'), max_length=10, blank=True, null=True)
    rendita2 = models.FloatField(_('Rendita'),blank=True, null=True)
    classe3 = models.CharField(_('Nr. Immobile'), max_length=10, blank=True, null=True)
    foglio3 = models.CharField(_('Foglio'), max_length=10, blank=True, null=True)
    particella3 = models.CharField(_('Particella'), max_length=10, blank=True, null=True)
    subalterno3 = models.CharField(_('Subalterno'), max_length=10, blank=True, null=True)
    categoria3 = models.CharField(_('Categoria'), max_length=10, blank=True, null=True)
    rendita3 = models.FloatField(_('Rendita'), blank=True, null=True)
    classe4 = models.CharField(_('Nr. Immobile'), max_length=10, blank=True, null=True)
    foglio4 = models.CharField(_('Foglio'), max_length=10, blank=True, null=True)
    particella4 = models.CharField(_('Particella'), max_length=10, blank=True, null=True)
    subalterno4 = models.CharField(_('Subalterno'), max_length=10, blank=True, null=True)
    categoria4 = models.CharField(_('Categoria'), max_length=10, blank=True, null=True)
    rendita4 = models.FloatField(_('Rendita'), blank=True, null=True)

    first_name = models.CharField(_('Nominativo'), max_length=255, blank=True, null=True)
    email = models.EmailField(_('Email'), blank=False, null=False)
    city = models.CharField(_('Comune'), max_length=255, blank=True, null=True)
    tel = models.CharField(_('Telefono'), max_length=255, blank=True, null=True)
    message = models.TextField(_('Messaggio'), blank=True, null=True)


    contratto = models.FileField(_('Allega il contratto (formato .doc, .pdf o immagine - Dim. max 5MB)'), upload_to='contacts/contratti', blank=False, null=False)
    visura = models.FileField(_('Documenti di riconoscimento (file .zip contenente tutti i soggetti coinvolti nel contratto - Dim. max 5MB)'), upload_to='contacts/contratti', blank=False, null=False)
    ape = models.FileField(_('APE - Certificazione energetica (formato .doc, .pdf o immagine - Dim. max 5MB)'), upload_to='contacts/contratti', blank=True, null=True)
    allegatigen = models.FileField(_('Allegati generici (file .zip contenente eventuali altri allegati generici - Dim. max 5MB)'), upload_to='contacts/contratti', blank=True, null=True)

    privacy_ok = models.BooleanField(_('privacy_accept'))
    newsletter = models.BooleanField(_('newsletter_accept'), blank=True)
    pagato = models.BooleanField(_('pagato'), blank=True, default=False)
    calcolodovuto = models.FloatField(_('Calcolo del dovuto'), blank=True, null=True)
    modalitapagamento = models.CharField(_('Modalita di pagamento'), max_length=255, blank=False, null=False, choices=CONTACT_TYPE_PAGA)

    nrprotoade = models.CharField(_('Protocollo Ade'), max_length=255, blank=True, null=True)
    ricevuta = models.FileField(_('Ricevuta Ade'), upload_to='contacts/contratti', blank=True,
                             null=True)

    coupon = models.CharField(_('Coupon'), max_length=255, blank=True, null=True)
    denoage = models.CharField(_('Denominazione Agenzia'), max_length=255, blank=True, null=True)


    class Meta:
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')
        ordering = ('dataprimascadenzacontratto', )


    def __unicode__(self):
        return '%s' % (self.first_name)
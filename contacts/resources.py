from import_export import resources, fields

from contacts.models import Contact


class ContactExportResource(resources.ModelResource):
    first_name = fields.Field(column_name='First name', attribute="first_name")
    #last_name = fields.Field(column_name='Last name', attribute="last_name")

    email = fields.Field(column_name='Email', attribute="email")
    tel = fields.Field(column_name='Phone', attribute="tel")

    city = fields.Field(column_name='City', attribute="city")
    creation_date = fields.Field(column_name='Date', attribute="creation_date")
    message = fields.Field(column_name='Message', attribute="message")

    class Meta:
        model = Contact
        exclude = ('id',)

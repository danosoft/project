from django.conf.urls import patterns, url

urlpatterns = patterns('',
                       url(r'^$', 'contacts.views.contact_form', name='contact_form'),
                       url(r'^branch_admin_visibility.js$', 'core.views.branch_admin_visibility_js',
                           name='branch_admin_visibility_js'),
                       url(r'^notify_changes/(?P<app_name>[\w+-]*)/(?P<model_name>[\w+-]*)/(?P<obj_pk>\d+)/',
                           'core.views.notify_changes', name='notify_changes'),
                       url(r'^notify_changes_calcdov/(?P<app_name>[\w+-]*)/(?P<model_name>[\w+-]*)/(?P<obj_pk>\d+)/',
                           'core.views.notify_changes_calcdov', name='notify_changes_calcdov'),
                       )

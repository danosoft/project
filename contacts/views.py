# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse, resolve
from django.shortcuts import render, redirect, render_to_response
from contacts.forms import ContactForm
from mailing.utils import get_data_table, send_email
from django.template import loader, Context, RequestContext
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from contacts.models import Contact
from core.utils import view_that_asks_for_money, view_that_asks_for_money_detail
from django.contrib.auth.decorators import login_required
from decimal import Decimal, getcontext
from django.http import HttpResponse
from django.shortcuts import render_to_response, get_object_or_404

@login_required
def contact_reg_paga(request, obj_pk):
    """
    Render a lista contatti inviati
    """
    template = 'contacts/detail_paga.html'
    current_user = request.user
    gtacont = get_object_or_404(Contact, pk=obj_pk)
    body_context = view_that_asks_for_money_detail(request, obj_pk, gtacont.calcolodovuto, gtacont.nome_registrazione);

    body_context.update({
        'gtacont': gtacont
    })

    return render(request, template, Context(body_context))

@login_required
def contact_reg(request):
    """
    Render a lista contatti inviati
    """
    template = 'contacts/detail.html'
    current_user = request.user
    print current_user.id
    gtacont = Contact.objects.filter(created_by=current_user).order_by('-creation_date')
    if request.user.is_authenticated():
        pass
    else:
        pass

    context = {
        'gtacont': gtacont
    }

    return render(request, template, context)

@login_required
def contact_form(request):
    """
    Handle the contact form

    When is valid send emails and save the contact

    :param request:
    :return:
    """
    current_app = resolve(request.path_info).namespace
    context = RequestContext(request, current_app=current_app)
    # contact_saved_url = reverse('contacts:contact_sent')
    if request.method == 'POST':
        form = ContactForm(request.POST, request.FILES, request=request)


        # if form.cleaned_data['datastipulacontratto'] > form.cleaned_data['datainiziolocazione']:
            # controlli sulle date con errore di ritorno
            # return False

        if form.is_valid():
            # calcolo del dobvuto
            contact = form.save()

            print 'Salvato'

            # FATTI PER IL CALCOLO NON SERVONO PIU
            # contact = form.save(commit=False)
            # ## calcolodovuto = (form.cleaned_data['canonetot'].__float__() * 2) + 3.2;
            # ## getcontext().prec = 10
            # ## calcolodovuto = (float(form.cleaned_data['canonetot']) * 2) + 3.2;
            # calcolodovuto = Decimal(((float(form.cleaned_data['canonetot']) * 2) + 3.2)).quantize(Decimal('0.01'),'ROUND_UP')
            # calcolodovuto = 0
            # form.cleaned_data['calcolodovuto'] = calcolodovuto
            # form.data['calcolodovuto'] = calcolodovuto
            # contact.calcolodovuto = calcolodovuto
            #
            # rendita = float(form.cleaned_data['rendita']) if form.cleaned_data['rendita'] else float(0)
            # form.cleaned_data['rendita'] = rendita
            # form.data['rendita'] = rendita
            # contact.rendita = rendita
            #
            # contact.save()

            print 'Assegno dati'

            # send log email to contact manager
            mail_message_exclude_fields = ['id', 'privacy', 'creation_date', 'changed_by', 'created_by', 'changed_date', 'pagato', 'nrprotoade', 'ricevuta', 'calcolodovuto']
            subject = _('immobilmatica - Riepilogo dati contratto inviato %s') % (contact.first_name)
            data_table = get_data_table(form, Contact._meta.fields, mail_message_exclude_fields)

            body_context = {
                'mail_title': subject,
                'data_table': data_table
            }
            t = loader.get_template('mailing/mail_data_table.html')
            body = t.render(Context(body_context))
            allegati = []
            if form.instance.contratto:
                allegati.append(form.instance.contratto.path)
            if form.instance.visura:
                allegati.append(form.instance.visura.path)

            print 'Invio'

            send_email(
                from_email=settings.DEFAULT_FROM_EMAIL,
                recipients=settings.DEFAULT_TO_EMAIL,
                subject=subject,
                body=body,
                cc=(form.cleaned_data['email'],),
                bcc=settings.DEFAULT_BCC_EMAIL,
                replyto=form.cleaned_data['email'],
                allegati=(allegati)
            )

            print 'Inviato'

            # assegno variabili di sessione
            request.session.set_expiry(1209600)
            request.session['nome_registrazione'] = form.cleaned_data['nome_registrazione']
            request.session['first_name'] = form.cleaned_data['first_name']
            request.session['coupon'] = form.cleaned_data['coupon']
            request.session['denoage'] = form.cleaned_data['denoage']
            request.session['email'] = form.cleaned_data['email']
            request.session['city'] = form.cleaned_data['city']
            request.session['tel'] = form.cleaned_data['tel']
            request.session['message'] = form.cleaned_data['message']
            request.session['elecodfislocatori'] = form.cleaned_data['elecodfislocatori']
            request.session['elecodfislocatari'] = form.cleaned_data['elecodfislocatari']
            request.session['rendita'] = form.cleaned_data['rendita']
            request.session['foglio'] = form.cleaned_data['foglio']
            request.session['particella'] = form.cleaned_data['particella']
            request.session['subalterno'] = form.cleaned_data['subalterno']
            request.session['categoria'] = form.cleaned_data['categoria']

            modpag = form.cleaned_data['modalitapagamento']
            if modpag=='ppc':
                context = view_that_asks_for_money(request, form, contact.pk);
                # return render(request, 'contacts/contact_sent.html', context)
                responseret = render_to_response('contacts/contact_sent.html', context)
                return responseret
            # TODO: send courtesy email to user
            # email = form.cleaned_data['email']

            # redirect
            next_url = request.POST.get('next')
            # if next_url:
            #     return redirect(next_url)
            context = {
                'form': form,
                'modpag': form.instance.get_modalitapagamento_display()
            }
            # return render(request, 'contacts/contact_sent.html', context)
            responseret = render_to_response('contacts/contact_sent.html', context)
            return responseret
    else:
        if request.user.denominazione:
            deno = request.user.denominazione
        else:
            deno = ('%s %s') % (request.user.last_name, request.user.first_name)
        initial = {
            'nome_registrazione': request.session.get('nome_registrazione'),
            'first_name': request.session.get('first_name') if request.session.get('first_name') else deno,
            'email': request.session.get('email') if request.session.get('email') else request.user.email,
            'city': request.session.get('city') if request.session.get('city') else request.user.citta,
            'tel': request.session.get('tel') if request.session.get('tel') else request.user.mobile,
            'message': request.session.get('message'),
            'elecodfislocatori': request.session.get('elecodfislocatori'),
            'elecodfislocatari': request.session.get('elecodfislocatari'),
            'classe': '1',
            'rendita': request.session.get('rendita'),
            'foglio': request.session.get('foglio'),
            'particella': request.session.get('particella'),
            'subalterno': request.session.get('subalterno'),
            'categoria': request.session.get('categoria'),
            'classe2': '2',
            'classe3': '3',
            'classe4': '4',
            'coupon': request.user.coupon,
            'denoage': request.user.denoage,
            'presgara': 'No',
            'cedosecca': 'No',
        }
        form = ContactForm(request=request, initial=initial)

    context = {
        'form': form,
        # 'next': contact_saved_url
    }

    return render(request, 'contacts/contact_form.html', context)


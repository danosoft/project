from django.contrib import admin
from import_export.admin import ExportMixin

from contacts.models import Contact, TipoContratti
from contacts.resources import ContactExportResource
from pleiadi_content.base.admin import ReadOnlyFieldsMixin
from django.db import models

# class AfterSendAdmin(admin.ModelAdmin):
#     def save_model(self, obj, nrprotoade):
#         obj.nrprotoade = request.nrprotoade
#         obj.save()

class BranchAdminVisibilityMixin(object):
    change_form_template = 'admin_overrides/branch_admin_visibility/change_form.html'

class TipoContrattiAdmin(admin.ModelAdmin):
    pass

class ContactAdmin(BranchAdminVisibilityMixin, ExportMixin, admin.ModelAdmin):
    # ReadOnlyFieldsMixin,
    resource_class = ContactExportResource
    list_display = ('first_name', 'email','pagato', 'calcolodovuto', 'dataprimascadenzacontratto', 'tiposervizio','nome_registrazione', 'coupon', 'nrprotoade', 'creation_date')
    date_hierarchy = 'creation_date'
    search_fields = ('pagato', 'tiposervizio', 'tipocontr', 'dataprimascadenzacontratto')
    list_filter = ['pagato']
    ordering = ('-creation_date',)
    exclude = ()

    fieldsets = (
        ('Dati Generici', {
            'fields': (
                'nome_registrazione',
                'first_name',
                'datastipulacontratto',
                'datainiziolocazione',
                'dataprimascadenzacontratto',
                'tiposervizio',
                'tipocontr',
                'tipocontratto',
                'nrcopie',
                'presgara',
                'codfisgara',
                'cedosecca',
                'notecedosecca',
                'canonetot',
                'email',
                'message',
                'elecodfislocatari',
                'elecodfislocatori',
                'city',
                'tel',
                'contratto',
                'visura',
                'ape', 'allegatigen',
                'newsletter',
                'privacy_ok',
                'coupon',
                'denoage',
            ),
            'classes': ('collapse',),
        }),
        ('Catasto', {
            'fields': (
                'classe',
                'rendita',
                'foglio',
                'particella',
                'subalterno',
                'categoria',
                'classe2',
                'rendita2',
                'foglio2',
                'particella2',
                'subalterno2',
                'categoria2',
                'classe3',
                'rendita3',
                'foglio3',
                'particella3',
                'subalterno3',
                'categoria3',
                'classe4',
                'rendita4',
                'foglio4',
                'particella4',
                'subalterno4',
                'categoria4',
            ),
            'classes': ('collapse',),
        }),
        ('Cedolare Secca', {
            'fields': (
                'numimsec', 'numloc', 'perposs', 'numimsec2', 'numloc2', 'perposs2',
                'numimsec3', 'numloc3', 'perposs3', 'numimsec4', 'numloc4', 'perposs4',
                'numimsec5', 'numloc5', 'perposs5', 'numimsec6', 'numloc6', 'perposs6',
                'numimsec7', 'numloc7', 'perposs7', 'numimsec8', 'numloc8', 'perposs8',
                'numimsec9', 'numloc9', 'perposs9', 'numimsec10', 'numloc10', 'perposs10',
            ),
            'classes': ('collapse',),
        }),
        ('Created', {
            'fields': (
                'created_by',
            ),
            'classes': ('collapse',),
        }),
        ('Calcolo del dovuto', {
            'fields': (
                'calcolodovuto',
            )
        }),
        ('Pagamento effettuato', {
            'fields': (
                'pagato',
            )
        }),
        ('Protocollo AdE', {
            'fields': (
                'nrprotoade',
                'ricevuta',
            )
        }),
    )

admin.site.register(Contact, ContactAdmin)
admin.site.register(TipoContratti, TipoContrattiAdmin)

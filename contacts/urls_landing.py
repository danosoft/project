from django.conf.urls import patterns, url
from contacts import views

urlpatterns = patterns('',
    url(r'^$', 'contacts.views.contact_reg', name='contact_reg'),
    url(r'^(?P<obj_pk>[\w+-]*)/$', 'contacts.views.contact_reg_paga', name='contact_reg_paga')
)
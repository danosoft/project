# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-10-17 14:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0011_contact_garanzie'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contact',
            name='garanzie',
        ),
        migrations.AddField(
            model_name='contact',
            name='categoria',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Categoria'),
        ),
        migrations.AddField(
            model_name='contact',
            name='classe',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Classe'),
        ),
        migrations.AddField(
            model_name='contact',
            name='comune',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Comune'),
        ),
        migrations.AddField(
            model_name='contact',
            name='foglio',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Foglio'),
        ),
        migrations.AddField(
            model_name='contact',
            name='particella',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Particella'),
        ),
        migrations.AddField(
            model_name='contact',
            name='rendita',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Rendita'),
        ),
        migrations.AddField(
            model_name='contact',
            name='sezione',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Sezione'),
        ),
        migrations.AddField(
            model_name='contact',
            name='subalterno',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Subalterno'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='canonetot',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True, verbose_name='Canone totale solo dei primi 12 mesi Euro'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='elecodfislocatari',
            field=models.TextField(blank=True, null=True, verbose_name='Elenco codici fiscale locatari (uno per riga)'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='elecodfislocatori',
            field=models.TextField(blank=True, null=True, verbose_name='Elenco codici fiscale locatori (uno per riga)'),
        ),
    ]

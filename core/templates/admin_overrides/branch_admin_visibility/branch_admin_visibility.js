/**
 * Created by enrico on 4/11/16.
 */

{% load static %}

        {% if obj_pk %}
            django.jQuery(document).ready(function () {
// alert('{{ obj_pk }}' + " - " + '{{ obj_nrprot }}')
                if ('{{ obj_nrprot }}'!='' && '{{ obj_nrprot }}'!='None'){
                    django.jQuery('.send_mail').hide();
                }

                {% if notify_changes_url %}
                    django.jQuery('.send_mail').on('click', function () {
                        if (django.jQuery('#id_nrprotoade').val()==''){
                            alert("Compilare il numero di protocollo AdE, per inviare la Notifica!");
                            return false;
                        }
                        //alert('{{ notify_changes_url }}?nrprotoade='+django.jQuery('#id_nrprotoade').val())
                        django.jQuery.ajax({
                            url: '{{ notify_changes_url }}?nrprotoade='+django.jQuery('#id_nrprotoade').val(),
                            type: 'GET',
                            beforeSend: function () {
                                django.jQuery('.send_mail').hide();
                                django.jQuery('<img class="loader" src = "{% static 'core/images/loading.gif' %}" width: "30px" height: "30px" style="position:absolute; left:500px;bottom:100px">').insertAfter('.send_mail');

                            },
                            success: function (data) {
                                django.jQuery('#popup-message-content').html(data.msg);
                                setTimeout(function () {
                                    django.jQuery('#popup-message-content').empty();
                                    // django.jQuery('.send_mail').show();
                                    django.jQuery('#popup-message-content').html('Notifica inviata con successo!')
                                }, 3000);
                            },
                            error: function () {
                            },
                            complete: function () {
                                django.jQuery('.loader').remove();

                            }


                        });

                    });
                {% else %}
                    django.jQuery('.send_mail').hide();
                {% endif %}


                {% if notify_changes_url_calcdov %}
                    django.jQuery('.send_mail_calcdov').on('click', function () {
                        if (django.jQuery('#id_calcolodovuto').val()==''){
                            alert("Compilare il Calcolo Dovuto, per inviare la Notifica!");
                            return false;
                        }
                        django.jQuery.ajax({
                            url: '{{ notify_changes_url_calcdov }}?calcolodovuto='+django.jQuery('#id_calcolodovuto').val(),
                            type: 'GET',
                            beforeSend: function () {
                                django.jQuery('.send_mail_calcdov').hide();
                                django.jQuery('<img class="loader" src = "{% static 'core/images/loading.gif' %}" width: "30px" height: "30px" style="position:absolute; left:500px;bottom:100px">').insertAfter('.send_mail_calcdov');
                            },
                            success: function (data) {
                                django.jQuery('#popup-message-content').html(data.msg);
                                setTimeout(function () {
                                    django.jQuery('#popup-message-content').empty();
                                    // django.jQuery('.send_mail').show();
                                    django.jQuery('#popup-message-content').html('Notifica Calcolo inviata con successo!')
                                }, 3000);
                            },
                            error: function () {
                            },
                            complete: function () {
                                django.jQuery('.loader').remove();

                            }


                        });

                    });
                {% else %}
                    django.jQuery('.send_mail').hide();
                {% endif %}

            });
        {% endif %}
import logging


class SuppressDeprecated(logging.Filter):
    """
    Suppress deprecation warnings.

    TODO: make it work
    """
    WARNINGS_TO_SUPPRESS = [
        'RemovedInDjango110Warning',
    ]

    def filter(self, record):
        # Return false to suppress message.
        return not any([warn in record.getMessage() for warn in self.WARNINGS_TO_SUPPRESS])

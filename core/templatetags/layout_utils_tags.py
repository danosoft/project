from django import template

register = template.Library()


@register.filter
def span_word(value, index='last'):
    response = value
    array = value.split(" ")
    if len(array) > 1:
        response = "%s " % array[0]
        array.pop(0)
        response += "<span>"
        for word in array:
            response += " %s" % word
        response += "</span>"

    return response


@register.filter
def only_domain_from_link(value):
    return value.strip('http://')
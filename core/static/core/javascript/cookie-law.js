(function () {
    var ITALIAN_TEXT = '<p>Questo sito fa uso di cookie per migliorare l\'esperienza di navigazione degli utenti e raccogliere informazioni sull\'utilizzo del sito stesso. Puoi conoscere i dettagli consultando la nostra privacy policy <a id="cookie-law-details-button" href="{{details_link}}">qui</a>.<br />Proseguendo nella navigazione si accetta l\'uso dei cookie; in caso contrario è possibile abbandonare il sito. <a id="cookie-law-button" href="#">OK</a></p>';
    var OTHER_TEXT = '<p>This site uses cookies to improve your navigation experience and to collect information on the way the site is used. For further details, see our data privacy policy <a id="cookie-law-details-button" href="{{details_link}}">here</a><br />By continuing to navigate this site, you implicitly accept our use of cookies. If you do not accept our use of cookies, you may leave this site now. <a id="cookie-law-button" href="#">OK</a></p>';

    var addEvent = function (element, evnt, funct) {
        if (element.attachEvent)
            return element.attachEvent('on' + evnt, funct);
        else
            return element.addEventListener(evnt, funct, false);
    };
    var setup = function () {
        var cookieLawDiv = document.getElementById('cookie-law'),
            cookieLawText,
            lng;
        lng = cookieLawLang.substring(0,2);
        if (lng === 'it') {
            cookieLawText = ITALIAN_TEXT;
        } else if (lng === 'fr') {
            cookieLawText = FRENCH_TEXT;
        } else if (lng === 'de') {
            cookieLawText = GERMAN_TEXT;
        } else if (lng === 'es') {
            cookieLawText = SPANISH_TEXT;
        } else if (lng === 'pt') {
            cookieLawText = BRASILIAN_TEXT;
        } else if (lng === 'tr') {
            cookieLawText = TURKISH_TEXT;
        } else if (lng === 'zh') {
            cookieLawText = CHINESE_TEXT;
        } else {
            cookieLawText = OTHER_TEXT;
        }
        cookieLawDiv.innerHTML = cookieLawText.replace(/{{details_link}}/g, '/' + lng + '/privacy/');
    };
    var getCookie = function (c_name) {
        var i, x, y, ARRcookies = document.cookie.split(";");
        for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
            x = x.replace(/^\s+|\s+$/g, "");
            if (x == c_name) {
                return unescape(y);
            }
        }
    };
    var setCookie = function (c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : ";path=/; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    };
    var toggleElement = function (element) {
        if (element.getAttribute('style') === 'display:none' || element.getAttribute('style') === 'display: none;' || element.getAttribute('style') === 'DISPLAY: none' || (typeof(element.getAttribute('style')) === 'object' && element.getAttribute('style').cssText === 'DISPLAY: none')) {
            if (typeof(element.getAttribute('style')) === 'object') {
                element.getAttribute('style').cssText = 'DISPLAY: block';
            } else {
                element.setAttribute('style', 'display:block');
            }
        } else {
            if (typeof(element.getAttribute('style')) === 'object') {
                element.getAttribute('style').cssText = 'DISPLAY: none';
            } else {
                element.setAttribute('style', 'display:none');
            }
        }
    };
    var checkCookie = function () {
        var cookieLawCookie = getCookie("cookieLawCookie"),
            cookieLawBar = document.getElementById('cookie-law');
        if (cookieLawCookie != null && cookieLawCookie != "") {
        } else {
            toggleElement(cookieLawBar);
        }
    };
    var cookieLawBarInit = function () {
        var button = document.getElementById('cookie-law-button'),
            cookieLawBar = document.getElementById('cookie-law');
        addEvent(button, 'click', function () {
            setCookie("cookieLawCookie", "CookiesAllowed", 365);
            toggleElement(cookieLawBar);
        });
    };
    var cookieLawBar = function () {
        setup();
        cookieLawBarInit();
        checkCookie();
    };

    //poor-man dom ready
    (function () {
        var tId = setInterval(function () {
            if (document.readyState == "complete") onComplete()
        }, 11);

        function onComplete() {
            clearInterval(tId);
            cookieLawBar();
        };
    })();
})();

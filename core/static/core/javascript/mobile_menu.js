$(function () {
    $("#mobile-menu").mmenu({
       "offCanvas": {
          "position": "right"
       },
       "extensions": [
          "theme-white"
       ],
       "navbar": {
          "title": ""
       },
       "navbars": [
          {
             "position": "top",
             "content": [
                "prev",
                "title",
                "close"
             ]
          }
       ]
    });

});
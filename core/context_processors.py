from django.contrib.sites.models import Site


def current_site(request):
    site = Site.objects.get_current()

    return {
        'current_site': site,
    }

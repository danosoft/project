from menus.base import NavigationNode


def _has_visible_children(node):
    if hasattr(node, 'children'):
        for child in node.children:
            if not child.is_hidden:
                return True

    return False


_patched = False


class MonkeyPatchMiddleware(object):
    def process_request(self, request):
        global _patched

        if not _patched:
            NavigationNode.has_visible_children = _has_visible_children

            _patched = True

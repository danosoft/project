from optparse import make_option
from cms.utils import copy_plugins
import re

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from modeltranslation.translator import translator
from cms.models import Page, Title
from django.contrib.auth.models import User
from cms.api import publish_page


class Command(BaseCommand):
    """
    Add new language in settings

    BACKUP YOUR DB (local and live)
    Do schemamigration for every single app
    Do migrate

    Than call this command.
    """
    can_import_settings = True
    help = "Command to create a new Localization Branch: createbranch -s source_language_code -d destination_language_code"
    option_list = BaseCommand.option_list + (
        make_option(
            "-s",
            "--source",
            dest="source_language_code",
            help="Specify the source language-code",
        ),
        make_option(
            "-d",
            "--destination",
            dest='destination_language_code',
            help='Specify the destination language-code',
        ),
        make_option(
            "-c",
            "--copy",
            dest='make_copy',
            help='Make just a copy of contents from source to destination language',
        ),
    )
    YES = ['y', 'yes']
    source_language_code = None
    destination_language_code = None
    make_copy = False
    translated_model_list = []

    def handle(self, *args, **options):
        # ===========================================================================
        # INIT
        # ===========================================================================
        available_languages = settings.LANGUAGES
        languages_codes = [l[0] for l in available_languages]

        self.translated_model_list = translator.get_registered_models()
        self.translated_app_list = list(set([model._meta.app_label for model in self.translated_model_list]))

        # command options
        self.source_language_code = options['source_language_code'] if options['source_language_code'] else None
        self.destination_language_code = options['destination_language_code'] if options[
            'destination_language_code'] else None
        self.make_copy = True

        if self.source_language_code is None:
            raise CommandError("You have to specify a source_language_code as command option: "
                               "createbranch -s source_language_code -d destination_language_code")
        if self.destination_language_code is None:
            raise CommandError("You have to specify a destination_language_code as command option: "
                               "createbranch -s source_language_code -d destination_language_code")

        # ==========================================
        # checking LANGUAGES settings
        # ==========================================
        if self.source_language_code not in languages_codes:
            raise CommandError("The given source_language must be in settings.LANGUAGES")
        if self.destination_language_code not in languages_codes:
            raise CommandError("The given destination_language must be in settings.LANGUAGES")
        if self.destination_language_code == self.source_language_code:
            raise CommandError("Source and Destination language codes must be different")

        go_ahead = raw_input('\nHave you made a backup of your database? (y/n): ')
        if not go_ahead in self.YES:
            exit()

        self.stdout.write("\n\nModel localization:\n")
        for app in self.translated_app_list:
            self.stdout.write("python manage.py schemamigration %(app_name)s --auto\n" % {'app_name': app})
        self.stdout.write("\npython manage.py migrate\n" % {'app_name': app})

        go_ahead = raw_input('\nHave you migrated all the localized apps? (y/n): ')
        if not go_ahead in self.YES:
            exit()

        self.stdout.write("""\n\n
====================================================================
Creating new Site language branch

Source language: %s

Destination language: %s
====================================================================\n"""
                          % (self.source_language_code, self.destination_language_code))

        go_ahead = raw_input('\nEverything looks good so far. Shall I go? (y/n): ')
        if not go_ahead in self.YES:
            exit()

        # ==========================================
        # COPY MODELS FIELDS
        # ==========================================
        self.copy_fields(self.source_language_code, self.destination_language_code)
        #self.copy_pages(self.source_language_code, self.destination_language_code)
        self.stdout.write('DONE!!!!!!!!!!!!!');
        exit()

    def copy_fields(self, source_language, destination_language):
        """
        Copy source_language localized field value to destination_language localized field
        for every field/model registered with modeltranslation

        :param source_language:
        :param destination_language:
        """
        models = translator.get_registered_models()

        self.stdout.write("COPY FIELDS\n"
                          "====================================================================\n\n")

        for model in models:
            # QUESTO TRY e PER EVITARE L'ERRORE :
            # AttributeError: type object SeoContent has no attribute objects
            # AttributeError: Manager isnt available; CoreContent is abstract
            try:
                self.stdout.write(
                    u'[%s.%s]\t %s' % (model._meta.app_label, model._meta.object_name, model._meta.db_table))
                instances = model.objects.all()
                for instance in instances:
                    try:
                        self.copy_fields_value(instance, source_language, destination_language)
                    except Exception, e:
                        self.stdout.write(u'ERROR: %s' % e)
            except Exception, e:
                self.stdout.write(u'ERROR: %s' % e)

    def copy_fields_value(self, instance, source_language, destination_language):
        """
        copy all the localized field values of instance from <field_name>_source-language
        to <field_name>_destination-language.

        :param instance:
        :param source_language:
        :param destination_language:
        """
        fields_to_copy = []

        # reformat the languages code with _ instead of -
        source_field_language_postfix = source_language.replace('-', '_')
        destination_field_language_postfix = destination_language.replace('-', '_')

        # looping over all the model field
        for field in instance._meta.fields:
            field_name = field.name

            # check if the current field is one of the destination language
            regexp = r'^(?P<clean_field_name>.*?)_%s$' % (destination_field_language_postfix,)
            regexp_match = re.match(regexp, field_name)

            if regexp_match:
                destination_field_name = field_name

                if not self.make_copy:
                    # check if the destination field already have value
                    if not getattr(instance, destination_field_name):
                        # getting the relative source language field
                        field_name_dict = regexp_match.groupdict()
                        source_field_name = '%s_%s' % (
                        field_name_dict['clean_field_name'], source_field_language_postfix,)
                        fields_to_copy.append({
                            'clean_field_name': field_name_dict['clean_field_name'],
                            'destination_field': destination_field_name,
                            'source_field': source_field_name
                        })
                else:
                    # getting the relative source language field
                    field_name_dict = regexp_match.groupdict()
                    source_field_name = '%s_%s' % (field_name_dict['clean_field_name'], source_field_language_postfix,)
                    fields_to_copy.append({
                        'clean_field_name': field_name_dict['clean_field_name'],
                        'destination_field': destination_field_name,
                        'source_field': source_field_name
                    })

        # executing the field content copy
        for item in fields_to_copy:
            try:
                setattr(instance, item['destination_field'], getattr(instance, item['source_field']))
                instance.save()
            except Exception, e:
                self.stdout.write("ERROR: %s -> %s\n%s\n" % (item['source_field'], item['destination_field'], e,))

    def copy_pages(self, source_language, destination_language):
        """

        For all the pages already translated in source_language you have to:

        Check if the page already have a page_title for destination_language, if it don't:

            Create a page title for destination_language and populate it
            Enable the page for the new site PageSites.sites.add(destination_site_id)
            For all the placeholders in the page copy_plugins from source_language

        @param request:
        @return:
        """
        self.stdout.write("COPY PAGES\n"
                          "====================================================================\n\n")
        #   WHY DRAFT??
        #
        # without .drafts() pages will contains even the public and the draft
        # version of the pages and will cause some difference between the source
        # and the destination page tree, depend on which version of the page
        # (public or draft) has been handled (and copied) first
        pages = Page.objects.all().drafts()

        # Select one page or testing purpose
        # pages = Page.objects.filter(pk=27).drafts()

        user = User.objects.get(pk=1)
        for page in pages:

            # check if source-language title object exist
            source_language_title_obj = get_localized_title(page, source_language)

            if not source_language_title_obj:
                # source-language title object doesn't exist
                # continue to next page
                self.stdout.write("\nPage %s (%s) doesn\'t exist in source language %s" % (
                    source_language_title_obj,
                    page.pk,
                    source_language,))
                continue

            self.stdout.write("\nPage %s (%s):" % (source_language_title_obj, page.pk))

            # check if destination-language title object already exist
            destination_language_title_obj = get_localized_title(page, destination_language)

            if destination_language_title_obj:

                if not self.make_copy:
                    # destination-language title object already exist
                    # continue to next page
                    self.stdout.write("\nAlready exist in destination language %s with the title \"%s\"" % (
                        destination_language,
                        destination_language_title_obj))
                    continue

            else:

                # destination-language title object doesn't exist
                try:
                    #   duplicate the source page title, app_hook (application_urls) included, for the new language
                    destination_language_title_obj = Title.objects.create(
                        language=destination_language,
                        title=source_language_title_obj.title,
                        page_title=source_language_title_obj.page_title,
                        menu_title=source_language_title_obj.menu_title,
                        slug=source_language_title_obj.slug,
                        application_urls=source_language_title_obj.application_urls,
                        redirect=source_language_title_obj.redirect,
                        meta_description=source_language_title_obj.meta_description,
                        meta_keywords=source_language_title_obj.meta_keywords,
                        page=source_language_title_obj.page
                    )
                    destination_language_title_obj.save()

                    self.stdout.write("\nTitle \"%s\" created for destination language %s" % (
                        destination_language_title_obj,
                        destination_language))

                except:
                    self.stdout.write("\n[Error] creating page %s (%s) in destination language %s<br />" % (
                        source_language_title_obj,
                        page.pk,
                        destination_language))

            try:
                self.stdout.write("\nCopy plugins\n\n")
                # For all the placeholders in the page copy_plugins from source_language
                copy_plugins_to_language(page, source_language, destination_language, not self.make_copy)
                # publish the page
                publish_page(page, user)
            except:
                self.stdout.write("\n[Error]")


def get_localized_title(page, language):
    # all the language versions of a page
    current_page_titles = list(page.title_set.all())
    for title in current_page_titles:
        if title.language == language:
            return title

    return None


def copy_plugins_to_language(page, source_language, target_language, only_empty=True):
    """
    Copy the plugins to another language in the same page for all the page
    placeholders.

    By default plugins are copied only if placeholder has no plugin for the
    target language; use ``only_empty=False`` to change this.
    Moreover if only_empty=False then the plugins in the target language will be deleted

    .. warning: This function skips permissions checks

    :param page: the page to copy
    :type page: :class:`cms.models.pagemodel.Page` instance
    :param string source_language: The source language code,
     must be in :setting:`django:LANGUAGES`
    :param string target_language: The source language code,
     must be in :setting:`django:LANGUAGES`
    :param bool only_empty: if False, plugin are copied even if
     plugins exists in the target language (on a placeholder basis).
    :return int: number of copied plugins
    """
    copied = 0
    placeholders = page.placeholders.all()
    for placeholder in placeholders:
        # only_empty is True we check if the placeholder already has plugins and
        # we skip it if has some

        if only_empty:

            if not placeholder.cmsplugin_set.filter(language=target_language).exists():
                plugins = list(
                    placeholder.cmsplugin_set.filter(language=source_language).order_by('tree_id', 'level', 'position'))
                copied_plugins = copy_plugins.copy_plugins_to(plugins, placeholder, target_language)
                copied += len(copied_plugins)

        else:
            # Firstly delete the exisiting plugins in target language from the page, since we are making a copy to an
            # existing page we do not want duplicates

            # print 'deleting plugins'
            old_plugins = list(
                placeholder.cmsplugin_set.filter(language=target_language).order_by('tree_id', 'level', 'position'))
            for p in old_plugins:
                p.delete_with_public()

            # print 'copying plugins'
            plugins = list(
                placeholder.cmsplugin_set.filter(language=source_language).order_by('tree_id', 'level', 'position'))
            copied_plugins = copy_plugins.copy_plugins_to(plugins, placeholder, target_language)
            copied += len(copied_plugins)

    return copied

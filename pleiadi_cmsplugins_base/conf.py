from django.conf import settings
from appconf import AppConf
from django.utils.translation import ugettext_lazy as _


class LinkConf(AppConf):
    NO_LINK = 'no_link'
    PAGE_LINK = 'page_link'
    NEWS_LINK = 'news_link'
    URL_LINK = 'url_link'
    MEDIA_LINK = 'media_link'

    LINK_TYPE_CHOICES = (
        (NO_LINK, _('No link')),
        (PAGE_LINK, _('Link to cms page')),
        (NEWS_LINK, _('Link to news detail page')),
        (URL_LINK, _('Link to web address')),
        (MEDIA_LINK, _('Link the media')),

    )

    DEFAULT_LINK_TYPE = NO_LINK

    class Meta:
        prefix = 'pleiadi_cmsplugins_link_type'

from cms.plugin_base import CMSPluginBase


class PluginBase(CMSPluginBase):
    """
    Does nothing, used as a common root class for all cms plugins
    """
    pass


class LinkMixinPlugin(object):
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        return super(LinkMixinPlugin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class ListBasePlugin(PluginBase):
    """
    Models handled by this kind ok plugin must define a title and media_items properties,

    *Plugin Model*
    The model that "contain" the media_items collection

    Your list plugins MUST extend pleiadi_filer_base.ListBase or one derived class
    Extending pleiadi_filer_base.ListBase gives you the title property and avoid copy_relations issue

    *Media Item Model*
    The media_items item model

    Your item model should extend pleiadi_filer_base.ListItemBase or one derived class
    media_items is typically the related name of the ForeignKey that link items objects to list object
    Every item listed in items_list should have a position property (used in sorting)
    """

    def render(self, context, instance, placeholder):
        context.update({
            'title': instance.title,
            'description': instance.description,
            'media': instance.media,
            'items_list': instance.media_items.order_by('position'),
        })
        return context

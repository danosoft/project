from django.forms.models import ModelForm
from django import forms


class LayoutMixinForm(ModelForm):
    """
    Base for for the plugins extending LayoutMixin

    Filling the template property choices according to the actual
    model extending the LayoutMixin
    """
    def __init__(self, *args, **kwargs):
        super(LayoutMixinForm, self).__init__(*args, **kwargs)
        self.fields['template'] = forms.ChoiceField(choices=self.instance.PLUGIN_TEMPLATES)
from django.conf import settings
from appconf import AppConf
from django.utils.translation import ugettext_lazy as _


class GalleryConf(AppConf):
    SLIDESHOW_STYLE_CHOICES = (
        ('slideshow_wide_banner', _('Wide Banner')),
    )
    FILELIST_STYLE_CHOICES = (
        ('filelist_downloads', _('Downloads')),
        ('filelist_imagegallery', _('Image pleiadi_cmsplugins_gallery')),
    )
    TEASERLIST_STYLE_CHOICES = (
        ('teaserlist_boxes', _('Boxes')),
    )

    DEFAULT_STYLE = 'slideshow_wide_banner'

    class Meta:
        prefix = 'pleiadi_cmsplugins_gallery'
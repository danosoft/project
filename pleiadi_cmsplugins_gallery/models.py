from django.db import models
from django.utils.translation import ugettext_lazy as _
from filer.fields.file import FilerFileField

from pleiadi_cmsplugins_base.fields import DimensionField
from pleiadi_cmsplugins_base.models import ListLinkedItemBase, ListBase
from pleiadi_content.base.models.fields import HtmlTextField
from pleiadi_cmsplugins_gallery.conf import settings


class GalleryBase(ListBase):
    """
    Extend the ListBase adding pleiadi_cmsplugins_gallery-specific properties
    """

    SLIDESHOW_TRANSITION_FX_FADE = 'fade'
    SLIDESHOW_TRANSITION_FX_SLIDE = 'slide'
    SLIDESHOW_TRANSITION_FX_CHOICES = (
        (SLIDESHOW_TRANSITION_FX_SLIDE, _('Slide'),),
        (SLIDESHOW_TRANSITION_FX_FADE, _('Fade'),),
    )

    SLIDESHOW_TRANSITION_SPEED_SLOW = '600'
    SLIDESHOW_TRANSITION_SPEED_NORMAL = '300'
    SLIDESHOW_TRANSITION_SPEED_FAST = '100'
    SLIDESHOW_TRANSITION_SPEED_CHOICES = (
        (SLIDESHOW_TRANSITION_SPEED_SLOW, _('Slow')),
        (SLIDESHOW_TRANSITION_SPEED_NORMAL, _('Normal')),
        (SLIDESHOW_TRANSITION_SPEED_FAST, _('Fast')),
    )

    timeout = models.IntegerField(_('pause between images'), default=4000, help_text=_('Set the time each slide stay in focus. Set it in milliseconds. Ex: 1000 means 1 second'))
    speed = models.CharField(_('transition speed'), max_length=8,
                             default=SLIDESHOW_TRANSITION_SPEED_NORMAL,
                             choices=SLIDESHOW_TRANSITION_SPEED_CHOICES,
                             help_text=_('How fast the slider do the transaction between two slide.'))
    fx = models.CharField(_('transition'), max_length=32,
                          default=SLIDESHOW_TRANSITION_FX_SLIDE,
                          choices=SLIDESHOW_TRANSITION_FX_CHOICES,
                          help_text=_('Which type of transition is used'))
    autoplay = models.BooleanField(_('autoplay'), default=False, help_text=_('Enable autoplay'))
    show_navigator = models.BooleanField(_('show navigation dots'), default=False, help_text=_('Show navigation dots to switch between slides.'))
    show_arrows = models.BooleanField(_('show prev/next arrows'), default=False, help_text=_('Show the prev/next arrows to move between slides.'))

    show_thumb = models.BooleanField(_('show thumb'), default=False, help_text=_('Show thumb to switch between slides.'))

    width = DimensionField(_('max. width'), null=True, blank=True,
                           help_text=_('You can leave this empty to use an automatically determined image width.'), default=1024)
    height = DimensionField(_('max. height'), null=True, blank=True,
                            help_text=_('You can leave this empty to use an automatically determined image height.'
                                        '<br /><strong>Width</strong> is intended to be 100% of the container'))

    GALLERY_STYLE_CHOICES = (
        (_('Slideshow'), settings.PLEIADI_CMSPLUGINS_GALLERY_SLIDESHOW_STYLE_CHOICES),
        (_('File List'), settings.PLEIADI_CMSPLUGINS_GALLERY_FILELIST_STYLE_CHOICES),
        (_('Teaser List'), settings.PLEIADI_CMSPLUGINS_GALLERY_TEASERLIST_STYLE_CHOICES),
    )
    style = models.CharField(_('Gallery Template'), max_length=255, choices=GALLERY_STYLE_CHOICES, default=settings.PLEIADI_CMSPLUGINS_GALLERY_DEFAULT_STYLE)

    class Meta:
        abstract = True
        verbose_name = _('pleiadi_cmsplugins_gallery pleiadi_cmsplugins_base')
        verbose_name_plural = _('pleiadi_cmsplugins_gallery pleiadi_cmsplugins_base')

    @property
    def fade(self):
        return self.fx == self.SLIDESHOW_TRANSITION_FX_FADE

    @property
    def template(self):
        return 'pleiadi_cmsplugins_gallery/gallery_%s.html' % (self.style, )

    @property
    def dimension(self):
        if self.width and self.height:
            return '%sx%s' % (self.width, self.height)

        return None


class PleiadiGallery(GalleryBase):
    """
    The actual Gallery plugin configuration model
    """
    class Meta:
        verbose_name = _('pleiadi pleiadi_cmsplugins_gallery')
        verbose_name_plural = _('pleiadi pleiadi_cmsplugins_gallery')


class PleiadiGalleryItem(ListLinkedItemBase):
    """
    Gallery Item
    """
    container_list = models.ForeignKey(PleiadiGallery, related_name='media_items')
    alt = models.CharField(_("alt"), max_length=255, null=True, blank=True,
                           help_text=_('Override the media image Alt.'))
    media = FilerFileField(null=True, blank=True)
    title = models.CharField(_('title'), max_length=255, blank=True, null=True,
                             help_text=_('Set a title for your pleiadi_cmsplugins_gallery item; It will be shown according to the selected template.'))
    sub_title = models.CharField(_('sub title'), max_length=255, blank=True, null=True,
                                 help_text=_('Set a sub-title for your pleiadi_cmsplugins_gallery item; It will be shown according to the selected template.'))
    description = HtmlTextField(_('description'), max_length=255, blank=True, null=True,
                                help_text=_('Set a description for your pleiadi_cmsplugins_gallery item; It will be shown according to the selected template.'))

    class Meta:
        ordering = ['position', 'id']
        verbose_name = _('pleiadi pleiadi_cmsplugins_gallery item')
        verbose_name_plural = _('pleiadi pleiadi_cmsplugins_gallery items')

    def __unicode__(self):
        unicode_parts = []
        if self.title:
            unicode_parts.append(self.title)

        if self.media:
            unicode_parts.append(self.media.name)

        if unicode_parts:
            return u', '.join(unicode_parts)

        return u'Gallery Item'

    @property
    def media_title(self):
        """
        Fallback logic for title, useful in a download file list.

        :return:
        """
        if not self.media:
            return ''

        return self.title or self.media
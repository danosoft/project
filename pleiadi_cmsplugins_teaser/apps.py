from django.apps import AppConfig


class PleiadiCmspluginsTeaserConfig(AppConfig):
    name = 'pleiadi_cmsplugins'

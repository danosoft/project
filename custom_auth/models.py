# coding: utf-8
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager
from django.core import validators
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class CustomAbstractUser(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username, password and email are required. Other fields are optional.
    """


    CONTACT_TYPE_CONTR_USER = (
        ('PF', _('Persona fisica')),
        ('PG', _('Ditta'))
    )


    tipouser = models.CharField(_('Tipo soggetto'), max_length=255, blank=True, null=True,
                             choices=CONTACT_TYPE_CONTR_USER)
    username = models.CharField(_('username'), max_length=30, unique=True,
                                help_text=_('Required. 30 characters or fewer. Letters, digits and '
                                            '@/./+/-/_ only.'),
                                validators=[
                                    validators.RegexValidator(r'^[\w.@+-]+$', _('Enter a valid username.'), 'invalid')
                                ])
    first_name = models.CharField(_('first name'), max_length=100, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    codfis = models.CharField(_('Codice Fiscale'), max_length=30, blank=True)
    pariva = models.CharField(_('Partita IVA'), max_length=30, blank=True)
    telefono = models.CharField(_('Telefono Fisso'), max_length=30, blank=True)
    mobile = models.CharField(_('Telefono Mobile'), max_length=30, blank=True)
    indirizzo = models.CharField(_('Indirizzo'), max_length=255, blank=True)
    citta = models.CharField(_('citta'), max_length=255, blank=True)
    cap = models.CharField(_('Fax'), max_length=30, blank=True)
    denominazione = models.CharField(_('Denominazione'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), blank=True)

    coupon = models.CharField(_('Coupon'), max_length=255, blank=True, null=True)
    denoage = models.CharField(_('Denominazione Ag. Immobiliare'), max_length=255, blank=True)
    codfisage = models.CharField(_('Codice Fiscale Ag. Immobiliare'), max_length=255, blank=True)


    privacy_ok = models.BooleanField(_('privacy_accept'), blank=True)
    news_ok = models.BooleanField(_('news_accept'), blank=True, default=True)

    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
        abstract = True

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


class CustomUser(CustomAbstractUser):
    def get_app_related_users(self, app_name, model_name):
        """
        App related users:

        are those user with access to the admin for <app> and, at least one different enabled language.

        [] (empty list) when:
            current user is superuser
            current user is not active
            current user have no staff privileges
            current user have no permission for <app>
            current user have no enabled languages

        Args:
            app_name:
            name of the app we want to check for related admin users

        Returns:

        """
        empty_result = []
        permissions_to_check = ['%s.change_%s' % (app_name, model_name)]  # ['%s.change_news' % app_name]

        # check for early return
        empty_result_conditions = self.is_superuser
        empty_result_conditions = empty_result_conditions or not self.is_active
        empty_result_conditions = empty_result_conditions or not self.is_staff
        empty_result_conditions = empty_result_conditions or not self.has_perms(permissions_to_check)

        # filtering related users
        users = CustomUser.objects.filter(is_staff=True, is_active=True, is_superuser=True)
        # .exclude(is_superuser=True).exclude(pk=self.pk)

        related_users = []
        for user in users:
            if user.has_perms(permissions_to_check):
                related_users.append(user)

        return related_users

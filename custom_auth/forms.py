# coding: utf-8
from django.contrib.auth.forms import UserCreationForm, PasswordResetForm, UserChangeForm
from custom_auth.models import CustomUser
from django import forms
from django.utils.translation import ugettext_lazy as _

class Registration(UserCreationForm):

    CONTACT_TYPE_CONTR_USER = (
        ('PF', _('Persona fisica')),
        ('PG', _('Ditta'))
    )

    email = forms.EmailField(required=True)
    # first_name = forms.CharField(required=True)
    # last_name = forms.CharField(required=True)
    privacy_ok = forms.BooleanField(required=True)

    mobile = forms.CharField(required=True)
    codfis = forms.CharField(required=True)
    tipouser = forms.ChoiceField(widget=forms.Select(attrs={"onChange": 'tipouserx(this)'}),
                                                    choices=CONTACT_TYPE_CONTR_USER)

    class Meta:
        model = CustomUser
        fields = ('username', 'first_name', 'last_name', 'email', 'telefono', 'mobile', 'indirizzo', 'citta', 'cap', 'denominazione', 'codfis', 'pariva', 'privacy_ok', 'news_ok', 'coupon', 'denoage', 'codfisage',)

class ProfilePasswordResetForm(PasswordResetForm):
    pass


class ProfileChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ("username", "first_name", "last_name", "email", 'telefono', 'mobile', 'indirizzo', 'citta', 'cap', 'denominazione', 'codfis', 'pariva', 'coupon', 'denoage', 'codfisage', 'news_ok')

    def clean_password(self):
        """
        password is not in the form so I can pass the validation
        """
        pass

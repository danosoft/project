from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.decorators.cache import never_cache

from custom_auth.forms import Registration, ProfileChangeForm


def create_user(request):

    if request.method == 'POST':
        form = Registration(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('user:complete_registration'))
    else:
        form = Registration()

    context = {
        'form': form
    }
    return render(request, 'custom_auth/user_creation.html', context)


def complete_registration(request):
    context = {
        'message_registration_ok': "Congratulazioni ti sei registrato correttamente, ora puoi procedere a loggarti."
    }
    return render(request, 'custom_auth/complete_registration.html', context)


def login_user(request):
    context = {}
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        next_url = request.POST.get('next', request.GET.get('next', ''))

        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                context = {
                    'user': user
                }
                return HttpResponseRedirect(next_url)

        else:
            context = {
                'message_login_fail': "Invalid username or password"
            }
    return render(request, 'core/style/default.html', context)


def logout_user(request):
    next_url = request.POST.get('next', request.GET.get('next', ''))
    logout(request)
    return HttpResponseRedirect(next_url)


@never_cache
@login_required(login_url="user:login_user")
def change_user(request):
    """
    Profile overview and change
    """
    form = ProfileChangeForm(request.POST or None, instance=request.user)
    data_saved = False

    if form.is_valid():
        form.save()
        data_saved = True

    return render(request, 'custom_auth/change_user_data.html', {
        'form': form,
        'data_saved': data_saved
    })

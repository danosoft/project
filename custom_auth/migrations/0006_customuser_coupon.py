# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-12-14 21:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_auth', '0005_auto_20161123_1734'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='coupon',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Coupon'),
        ),
    ]

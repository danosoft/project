# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2017-01-24 13:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_auth', '0007_auto_20170116_1006'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='citta',
            field=models.CharField(blank=True, max_length=255, verbose_name='citta'),
        ),
        migrations.AlterField(
            model_name='customuser',
            name='first_name',
            field=models.CharField(blank=True, max_length=100, verbose_name='nome'),
        ),
        migrations.AlterField(
            model_name='customuser',
            name='indirizzo',
            field=models.CharField(blank=True, max_length=255, verbose_name='Indirizzo'),
        ),
        migrations.AlterField(
            model_name='customuser',
            name='last_name',
            field=models.CharField(blank=True, max_length=100, verbose_name='cognome'),
        ),
    ]

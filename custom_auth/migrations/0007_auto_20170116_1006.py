# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2017-01-16 09:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_auth', '0006_customuser_coupon'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customuser',
            name='citta',
        ),
        migrations.AddField(
            model_name='customuser',
            name='codfis',
            field=models.CharField(blank=True, max_length=30, verbose_name='Codice Fiscale'),
        ),
        migrations.AddField(
            model_name='customuser',
            name='denominazione',
            field=models.CharField(blank=True, max_length=30, verbose_name='Denominazione'),
        ),
        migrations.AddField(
            model_name='customuser',
            name='mobile',
            field=models.CharField(blank=True, max_length=30, verbose_name='Telefono Mobile'),
        ),
        migrations.AddField(
            model_name='customuser',
            name='news_ok',
            field=models.BooleanField(default=True, verbose_name='news_accept'),
        ),
        migrations.AddField(
            model_name='customuser',
            name='pariva',
            field=models.CharField(blank=True, max_length=30, verbose_name='Partita IVA'),
        ),
        migrations.AddField(
            model_name='customuser',
            name='tipouser',
            field=models.CharField(blank=True, choices=[(b'PF', 'Persona fisica'), (b'PG', 'Ditta')], max_length=255, null=True, verbose_name='Tipo soggetto'),
        ),
        migrations.AlterField(
            model_name='customuser',
            name='cap',
            field=models.CharField(blank=True, max_length=30, verbose_name='Fax'),
        ),
        migrations.AlterField(
            model_name='customuser',
            name='telefono',
            field=models.CharField(blank=True, max_length=30, verbose_name='Telefono Fisso'),
        ),
    ]

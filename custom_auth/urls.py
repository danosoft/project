from django.conf.urls import patterns, url

from custom_auth import views
from custom_auth.forms import ProfilePasswordResetForm


urlpatterns = patterns('',
                       url(r'^create/$', views.create_user, name='create_user'),
                       url(r'^login/$', views.login_user, name='login_user'),
                       url(r'^complete/$', views.complete_registration, name='complete_registration'),

                       url(r'^logout/$', views.logout_user, name='logout_user'),

                       url(r'^profile/$', views.change_user, name='change_user_data'),

                       url(r'^password_reset/$', 'django.contrib.auth.views.password_reset', {
                           'template_name': 'custom_auth/password_reset_form.html',
                           'post_reset_redirect': 'user:password_reset_done',
                           'email_template_name': 'custom_auth/password_reset_email.html',
                           'password_reset_form': ProfilePasswordResetForm
                       }, name='password_reset'),

                       url(r'^password-reset/done/$', 'django.contrib.auth.views.password_reset_done', {
                           'template_name': 'custom_auth/password_reset_done.html'
                       }, name='password_reset_done'),

                       url(r'^reset/(?P<uidb64>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
                           'django.contrib.auth.views.password_reset_confirm',
                           {
                               'template_name': 'custom_auth/password_reset_confirm.html',
                               'post_reset_redirect': 'user:password_reset_complete'
                           }, name='password_reset_confirm'),

                       url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete',
                           {'template_name': 'custom_auth/password_reset_complete.html'},
                           name='password_reset_complete'),

                       url(r'^password-change/$', 'django.contrib.auth.views.password_change', {
                           'post_change_redirect': 'user:password_change_done',
                           'template_name': 'custom_auth/change_user_password.html',
                       }, name='change_user_password'),

                       url(r'^password-change/done/$', 'django.contrib.auth.views.password_change_done', {
                           'template_name': 'custom_auth/password_change_done.html',
                       }, name='password_change_done'),

                       )


from django.db import models
from django.utils.translation import ugettext_lazy as _
from pleiadi_content.base.models.fields import AutoSlugField


class Country(models.Model):
    name = models.CharField(_('Country name'), max_length=128)
    slug = AutoSlugField(_('slug'))
    numeric_code = models.IntegerField(_('numeric code'), unique=True)
    alpha_2_code = models.CharField(_('alpha 2 code'), max_length=2, primary_key=True, unique=True,
                                    help_text="Two Chars Country Code (IT, GB, ecc...)")
    alpha_3_code = models.CharField(_('alpha 3 code'), max_length=3,
                                    help_text="Two Chars Country Code (ITA, GBR, ecc...)")
    iso = models.CharField(_('ISO 3166'), max_length=20, help_text="Two Chars Country Code (ITA, GBR, ecc...)")

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')
        ordering = ('name',)

    def __unicode__(self):
        return u'%s' % (self.name, )


class Region(models.Model):
    name = models.CharField(_('Region name'), max_length=128)
    slug = AutoSlugField(_('slug'))
    country = models.ForeignKey(Country, verbose_name=_("Country"), null=True)

    class Meta:
        verbose_name = _('Region')
        verbose_name_plural = _('Regions')
        ordering = ('name',)

    def __unicode__(self):
        return u'%s' % (self.name, )


class Province(models.Model):
    name = models.CharField(_('Province name'), max_length=128)
    slug = AutoSlugField(_('slug'))
    region = models.ForeignKey(Region, verbose_name=_("Region"), null=True)

    class Meta:
        verbose_name = _('Province')
        verbose_name_plural = _('Provinces')
        ordering = ('region', 'name')

    def __unicode__(self):
        return u'%s' % (self.name, )
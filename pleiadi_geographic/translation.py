from modeltranslation.translator import translator, TranslationOptions
from pleiadi_geographic.models import Country, Region, Province


class CountryTranslationOptions(TranslationOptions):
    fields = ('name', 'slug',)


class RegionTranslationOptions(TranslationOptions):
    fields = ('name', 'slug',)


class ProvinceTranslationOptions(TranslationOptions):
    fields = ('name', 'slug',)
        
translator.register(Country, CountryTranslationOptions)
translator.register(Region, RegionTranslationOptions)
translator.register(Province, ProvinceTranslationOptions)
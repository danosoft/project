# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import pleiadi_content.base.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('name', models.CharField(max_length=128, verbose_name='Country name')),
                ('name_it', models.CharField(max_length=128, null=True, verbose_name='Country name')),
                ('name_en', models.CharField(max_length=128, null=True, verbose_name='Country name')),
                ('slug', pleiadi_content.base.models.fields.AutoSlugField(help_text='The item slice of the URL', verbose_name='slug', blank=True)),
                ('slug_it', pleiadi_content.base.models.fields.AutoSlugField(blank=True, help_text='The item slice of the URL', null=True, verbose_name='slug')),
                ('slug_en', pleiadi_content.base.models.fields.AutoSlugField(blank=True, help_text='The item slice of the URL', null=True, verbose_name='slug')),
                ('numeric_code', models.IntegerField(unique=True, verbose_name='numeric code')),
                ('alpha_2_code', models.CharField(primary_key=True, serialize=False, max_length=2, help_text=b'Two Chars Country Code (IT, GB, ecc...)', unique=True, verbose_name='alpha 2 code')),
                ('alpha_3_code', models.CharField(help_text=b'Two Chars Country Code (ITA, GBR, ecc...)', max_length=3, verbose_name='alpha 3 code')),
                ('iso', models.CharField(help_text=b'Two Chars Country Code (ITA, GBR, ecc...)', max_length=20, verbose_name='ISO 3166')),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': 'Country',
                'verbose_name_plural': 'Countries',
            },
        ),
        migrations.CreateModel(
            name='Province',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='Province name')),
                ('name_it', models.CharField(max_length=128, null=True, verbose_name='Province name')),
                ('name_en', models.CharField(max_length=128, null=True, verbose_name='Province name')),
                ('slug', pleiadi_content.base.models.fields.AutoSlugField(help_text='The item slice of the URL', verbose_name='slug', blank=True)),
                ('slug_it', pleiadi_content.base.models.fields.AutoSlugField(blank=True, help_text='The item slice of the URL', null=True, verbose_name='slug')),
                ('slug_en', pleiadi_content.base.models.fields.AutoSlugField(blank=True, help_text='The item slice of the URL', null=True, verbose_name='slug')),
            ],
            options={
                'ordering': ('region', 'name'),
                'verbose_name': 'Province',
                'verbose_name_plural': 'Provinces',
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='Region name')),
                ('name_it', models.CharField(max_length=128, null=True, verbose_name='Region name')),
                ('name_en', models.CharField(max_length=128, null=True, verbose_name='Region name')),
                ('slug', pleiadi_content.base.models.fields.AutoSlugField(help_text='The item slice of the URL', verbose_name='slug', blank=True)),
                ('slug_it', pleiadi_content.base.models.fields.AutoSlugField(blank=True, help_text='The item slice of the URL', null=True, verbose_name='slug')),
                ('slug_en', pleiadi_content.base.models.fields.AutoSlugField(blank=True, help_text='The item slice of the URL', null=True, verbose_name='slug')),
                ('country', models.ForeignKey(verbose_name='Country', to='pleiadi_geographic.Country', null=True)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': 'Region',
                'verbose_name_plural': 'Regions',
            },
        ),
        migrations.AddField(
            model_name='province',
            name='region',
            field=models.ForeignKey(verbose_name='Region', to='pleiadi_geographic.Region', null=True),
        ),
    ]

from models import Country, Region, Province
from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TabbedExternalJqueryTranslationAdmin
from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext_lazy as _


class RegionByCountryFilter(SimpleListFilter):

    title = _('country')
    parameter_name = 'country'

    def lookups(self, request, model_admin):
        countries = {c.country for c in model_admin.model.objects.all() if c.country}
        country_list = [country for country in countries]
        country_list.sort(key=lambda x: x.name, reverse=False)
        return [(c.pk, c.name) for c in country_list]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(country=self.value())
        else:
            return queryset


class ProvinceByRegionFilter(SimpleListFilter):

    title = _('region')
    parameter_name = 'region'

    def lookups(self, request, model_admin):
        regions = {i.region for i in model_admin.model.objects.all() if i.region}
        region_list = [region for region in regions]
        region_list.sort(key=lambda x: x.name, reverse=False)
        return [(r.pk, r.name) for r in region_list]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(region=self.value())
        else:
            return queryset


class CountryAdmin(TabbedExternalJqueryTranslationAdmin):
    list_display = ('name', 'numeric_code', 'alpha_2_code', 'alpha_3_code', 'iso')
    search_fields = list_display


class RegionAdmin(TabbedExternalJqueryTranslationAdmin):
    list_display = ('name', 'country', )
    search_fields = list_display
    list_filter = (RegionByCountryFilter, )


class ProvinceAdmin(TabbedExternalJqueryTranslationAdmin):
    list_display = ('name', 'region', )
    search_fields = list_display
    list_filter = (ProvinceByRegionFilter,)

admin.site.register(Country, CountryAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(Province, ProvinceAdmin)
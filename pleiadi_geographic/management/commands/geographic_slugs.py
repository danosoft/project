from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from django.db import connections
from django.db.models import get_app, get_models
from django.utils.text import slugify
from django.utils.translation import get_language, activate, deactivate
from pleiadi_geographic.models import Country
from django.conf import settings

class Command(BaseCommand):
    help = "command to generate slugs for pleiadi_geographic model/s"
    option_list = BaseCommand.option_list + (

        make_option(
            "-m",
            "--model",
            dest="modelname",
            help="specify the model",
        ),
        make_option(
            "-f",
            "--force",
            action='store_true',
            dest='force',
            default=False,
            help='Force slug update',
        ),
    )

    def handle(self, database="default", *args, **options):
        app = get_app('pleiadi_geographic')
        modellist = get_models(app)

        if not modellist:
            raise CommandError("current app has no models")

        # make sure file option is present
        if options['modelname'] is not None and options['modelname'] not in modellist:
            raise CommandError("the specified model is not part of the current app")


        current_language = get_language()
        languages = settings.LANGUAGES
        items = Country.objects.all()

        for lang, label in languages:
            activate(lang)
            for item in items:
                if not item.slug or options['force']:
                    item.slug = slugify(item.name)
                    item.save()

        activate(current_language)
                    # app = get_app('pleiadi_geographic')
        # for model in get_models(app):
        #
        #     items = model.objects.all()
        #     for item in items:
        #         item.slug = slugify(item.name)
        #         item.save()
gettext = lambda s: s

CMS_TEMPLATES = (
    ('cms/pages/homepage.html', gettext('Homepage')),
    ('cms/pages/default.html', gettext('Default page')),
)


CMS_LANGUAGES = {

    'default': {
        'fallbacks': [],
        'redirect_on_fallback': True,
        'public': True,
        'hide_untranslated': True,
    }
}


from django import template

register = template.Library()


def rel_alternate(context):
    """
    !!!!!!What!!! And Why I need something like this?!!!!!!

    It looks it's not doing anything.

    Well it's rendering the alternates urls with a template.. maybe it's still useful.

    :param context:
    :return:
    """

    try:
        alternates = context['rel_alternate']
    except KeyError:
        alternates = []

    return {
        'rel_alternate': alternates,
    }

register.inclusion_tag('pleiadi_content/seo/rel_alternate.html', takes_context=True)(rel_alternate)
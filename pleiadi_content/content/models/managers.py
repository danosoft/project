from django.utils.timezone import now
from modeltranslation.manager import MultilingualManager
from django.contrib.sites.managers import CurrentSiteManager
from django.db.models import Q


class VisibilityManager(MultilingualManager, CurrentSiteManager):
    """
    Returns active and visible instances.

    Base visibility manager: return instances which satisfy all the visibility condition.

    Visibility Conditions
    ---------------------
    Global Visibility: active=True
    Language Visibility: visible=True (visible is a translated field)
    Site: handled by the CurrentSiteManager
    """
    def get_queryset(self):
        return super(VisibilityManager, self).get_queryset().filter(active=True, visible=True)


class DateValidityManager(VisibilityManager):
    """
    Return instances with valid date.

    Check the validity date of the instances after all the visibility condition has been verified (by the VisibilityManager)
    """
    def get_queryset(self):
        return super(DateValidityManager, self).get_queryset().filter(
            Q(valid_from__isnull=True) |
            Q(valid_from__lte=now(), valid_to__isnull=True) |
            Q(valid_from__lte=now(), valid_to__gte=now())
        )


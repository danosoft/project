from django.contrib.sites.models import Site
from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from filer.fields.image import FilerImageField

from pleiadi_content.base.models.base import BaseModel
from pleiadi_content.base.models.fields import HtmlTextField
from pleiadi_content.content.models.managers import VisibilityManager, DateValidityManager
from pleiadi_content.seo.models import SeoMixin, RelAlternateMixin


class VisibilityMixin(models.Model):
    """
    Add to a model the fields and managers for handling the visibility logic.
    """
    active = models.BooleanField(_('Global visibility'),
                                 help_text=_('Make this content visible according to Site and Language visibility'),
                                 default=False)
    visible = models.BooleanField(_('Language visibility'), help_text=_('Make this content visible for this language'),
                                  default=False)
    sites = models.ManyToManyField(Site, verbose_name=_('Site visibility'), blank=True,
                                   related_name="%(app_label)s_%(class)s_related",
                                   help_text=_('Make this content visible for selected sites '
                                               'according to Global and Language visibility'))

    class Meta:
        abstract = True

    def is_visible(self):
        """
        This method should logically live inside the VisibilityMixin which define the three visibility-fields used in
        the code but is a SeoMixin method and you have to define it in a model extending the SeoMixin.
        """
        try:
            visible = super(VisibilityMixin, self).is_visible()
        except (AttributeError, NotImplementedError):
            visible = True

        return visible and self.active and self.visible and (Site.objects.get_current() in self.sites.all())
    is_visible.boolean = True


class DateValidityMixin(models.Model):
    """
    Add to a model the fields and managers for handling the Date Validity logic.

    See DateValidityManager for the implementation of the date validity logic.
    """
    valid_from = models.DateTimeField(auto_now_add=False, blank=True, null=True)
    valid_to = models.DateTimeField(auto_now_add=False, blank=True, null=True)

    class Meta:
        abstract = True

    def is_visible(self):
        """
        This method should logically live inside the DateValidityMixin which define the two validity-fields used in
        the code but is a SeoMixin method and you have to define it in a model extending the SeoMixin.
        """
        try:
            visible = super(DateValidityMixin, self).is_visible()
        except (AttributeError, NotImplementedError):
            visible = True

        if visible:
            valid = self.valid_from is None
            valid = valid or self.valid_from <= now() and self.valid_to is None
            valid = valid or self.valid_from <= now() <= self.valid_to

            return valid

        return False
    is_visible.boolean = True


class AdministrativeMixin(models.Model):
    """
    Field for administrative data of the content
    """
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="%(app_label)s_%(class)s_owner_related")
    changed_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="%(app_label)s_%(class)s_changed_by_related")
    creation_date = models.DateTimeField(auto_now_add=True)
    changed_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class BaseContent(VisibilityMixin, SeoMixin, RelAlternateMixin, AdministrativeMixin, BaseModel):
    """
    An abstract pleiadi_cmsplugins_base model for all the contents of the site

    BaseModel: Fields and logic for every title+slug content.
    AdministrativeMixin: A content must have a owner and must store information about when it was created and changed.
    RelAlternateMixin: Every model with a detail must supply SEO rel_alternate information.
    SeoMixin: Every model with a detail must supply SEO meta information.
    VisibilityMixin: Every model with a detail can be visible at different levels; site, language, global.
    """
    image = FilerImageField(blank=True, null=True, related_name="+", help_text=_('Main image of your content'))
    image_alt = models.CharField(_('Image alternate text'), blank=True, null=True, max_length=255,
                                 help_text=_('the alternate text for the main image of the news.'
                                             '<br />Mandatory for SEO.'))
    description = HtmlTextField(_('description'), blank=True, null=True)
    abstract = HtmlTextField(_('abstract'), blank=True, null=True)

    objects = models.Manager()
    visible_on_site = VisibilityManager()

    class Meta:
        abstract = True

    def seo_fallback_fields(self):
        """
        BaseContent is the first Class, hierarchically speaking, with SEO information cause is the first who extend
        SeoMixin even if the title property is defined in BaseModel.
        """
        seo_fallbacks = super(BaseContent, self).seo_fallback_fields()
        seo_fallbacks.update({
            'seo_title': self.title,
            'seo_description': self.description,
        })

        return seo_fallbacks


class BaseContentDateValidity(DateValidityMixin, BaseContent):
    """
    A pleiadi_cmsplugins_base content with date-based validity fields and logic
    """
    objects = models.Manager()
    visible_on_site = DateValidityManager()

    class Meta:
        abstract = True
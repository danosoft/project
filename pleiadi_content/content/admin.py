from django.contrib.sites.models import Site
from django import forms
from django.core.exceptions import ObjectDoesNotExist

from django.utils.translation import ugettext_lazy as _
from modeltranslation.admin import TabbedTranslationAdmin
from pleiadi_content.base.admin.base import FieldsetsMixin, TranslationMixin

from pleiadi_content.seo.admin import SeoAdminMixin


class VisibilityAdminMixin(object):
    """
    Defines constants for fieldsets and display_list concerning the visibility of the Model instance.
    """
    visibility_fieldset = ('Visibility', {
        'classes': ['collapse'],
        'fields': ['active', 'visible', 'sites']
    })

    visibility_display_list = ['active', 'visible']

    def get_form(self, request, obj=None, **kwargs):
        """
        Set the initial values of some fields

        :param request:
        :param obj:
        :param kwargs:
        :return:
        """
        form = super(VisibilityAdminMixin, self).get_form(request, obj, **kwargs)

        if 'active' in form.base_fields and obj is None:
            form.base_fields['active'].initial = True

        # Changing widget and default value of site field
        if 'sites' in form.base_fields:
            form.base_fields['sites'].widget = forms.CheckboxSelectMultiple()
            form.base_fields['sites'].help_text = _('Make this content visible for selected sites according to '
                                                    'Global and Language visibility')

            # always select current site for new items
            if obj is None:
                form.base_fields['sites'].initial = [Site.objects.get_current()]

        return form


class AdministrativeAdminMixin(object):
    """
    Defines constants for fieldsets concerning who own the Model instance.
    """
    admin_fieldset = ('Admin', {
        'classes': ['collapse'],
        'fields': [
            ('created_by', 'creation_date'),
            ('changed_by', 'changed_date')
        ]
    })
    readonly_fields = ('created_by', 'changed_by', 'creation_date', 'changed_date')

    def save_model(self, request, obj, form, change):
        """
        Deal with owner

        :param request:
        :param obj:
        :param form:
        :param change:
        :return:
        """
        created_by = None
        try:
            created_by = getattr(obj, 'created_by')
        except ObjectDoesNotExist:
            pass
        if created_by is None:
            obj.created_by = request.user

        try:
            getattr(obj, 'changed_by')
        except ObjectDoesNotExist:
            pass
        obj.changed_by = request.user

        super(AdministrativeAdminMixin, self).save_model(request, obj, form, change)


class BaseContentAdmin(FieldsetsMixin, VisibilityAdminMixin, SeoAdminMixin, TranslationMixin, AdministrativeAdminMixin,
                       TabbedTranslationAdmin):

    readonly_fields = ('created_by', 'changed_by', 'creation_date', 'changed_date')

    base_content_fieldset = (None, {
        'fields': ('title', 'slug', 'abstract', 'description', ('image', 'image_alt'), )
    })
    content_fieldset = base_content_fieldset
    base_content_display_list = ['admin_title', 'translations']


class DateValidityAdminMixin(object):
    """
    Defines constants for fieldsets and list_display
    """
    date_validity_fieldset = ('Date validity', {
        'classes': ['collapse'],
        'fields': ['valid_from', 'valid_to']
    })

    date_validity_display_list = ['valid_from', 'valid_to']


class DateValidityBaseContentAdmin(DateValidityAdminMixin, BaseContentAdmin):
    def get_fieldsets(self, request, obj=None):
        """
        Returns a fully populated fieldsets with fields of all the ancestors classes.

        :param request:
        :param obj:
        :return:
        """
        if not self.fieldsets:
            self.fieldsets = (
                self.seo_fieldset,
                self.content_fieldset,
                self.visibility_fieldset,
                self.date_validity_fieldset,
                self.admin_fieldset
            )

        return super(DateValidityBaseContentAdmin, self).get_fieldsets(request, obj)
from django.db import models
from pleiadi_content.base.models.fields import AutoSlugField
from django.utils.translation import ugettext_lazy as _


class BaseModel(models.Model):
    """
    Pleiadi pleiadi_cmsplugins_base model, title and slug are the basic information of every model.

    Title is optional to avoid problem in multilingual sites.
    """
    BASEMODEL_TITLE_LABEL = _('title')
    BASEMODEL_TITLE_HELPTEXT = _('Title of your content')

    title = models.CharField(verbose_name=BASEMODEL_TITLE_LABEL, max_length=200, blank=True, null=True,
                             help_text=BASEMODEL_TITLE_HELPTEXT)
    slug = AutoSlugField(populate_from='title')

    class Meta:
        abstract = True
        ordering = ['title']

    def __unicode__(self):
        return u'%s' % (self.title, )


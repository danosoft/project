import re

from django.forms.widgets import Textarea, TextInput
from django.utils.safestring import mark_safe
from modeltranslation.utils import build_localized_fieldname
from django.template.loader import render_to_string


class HtmlTextEditorWidget(Textarea):
    """
    Used to add HtmlText specific attribute to the Textarea widget
    """
    class Media(object):
        css = {
            'all': ('pleiadi_content/css/htmltext_editor_widget.css',)
        }
        js = (
            'ckeditor/ckeditor.js',
            'pleiadi_content/js/htmltext_editor_widget.js'
        )

    def __init__(self, attrs=None):
        default_attrs = {'class': 'htmltext-editor-widget'}
        if attrs:
            default_attrs.update(attrs)
        super(HtmlTextEditorWidget, self).__init__(default_attrs)


class AutoSlugWidget(TextInput):
    """
    Renders the slug field with a reload slug button. The reload slug button generate the new slug from field defined
    in populate_from parameter.
    """
    populate_from = ''

    class Media(object):
        css = {
            'all': ('pleiadi_content/css/autoslug_widget.css',)
        }
        js = (
            'pleiadi_content/js/autoslug_widget.js',
        )

    def __init__(self, attrs=None, populate_from=''):
        self.populate_from = populate_from

        default_attrs = {'class': 'autoslug'}
        if attrs:
            default_attrs.update(attrs)
        super(AutoSlugWidget, self).__init__(default_attrs)

    def render(self, name, value, attrs=None):
        super_html = super(AutoSlugWidget, self).render(name, value, attrs)

        if self.populate_from:
            self.media.add_js(('pleiadi_content/js/slug_reload.js',))

            slug_field_id = attrs.get('id', '') if attrs else ''
            destination_field_id = '#%s' % slug_field_id

            re_search_language_postfix = re.search(r'_([^_]*)$', slug_field_id)

            is_localized = re_search_language_postfix is not None
            if is_localized:
                current_field_language = re_search_language_postfix.groups()[0]

                source_field_id = '#id_%s' % build_localized_fieldname(self.populate_from, current_field_language)
            else:
                source_field_id = '#id_%s' % self.populate_from

            widget_contex = {
                'source_field_id': source_field_id,
                'destination_field_id': destination_field_id
            }
            reload_slug_html = render_to_string('pleiadi_content/widget/slug_reloader.html', widget_contex)

            html = "%s%s" % (super_html, reload_slug_html)
        else:
            html = super_html

        return mark_safe(html)
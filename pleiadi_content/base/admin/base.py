from django.utils.translation import ugettext_lazy as _
from django.templatetags.static import static
from django.core.urlresolvers import reverse
from django.utils import translation
from django.utils.safestring import mark_safe
from pleiadi_content.languages import Language
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.exceptions import InvalidImageFormatError


class ListImageMixin(object):
    list_image_field_name = 'image'
    list_image_size = '100x100'

    def get_image_field(self, obj):
        return getattr(obj, self.list_image_field_name, None)

    def admin_image(self, obj):
        image_url = 'http://placehold.it/%s' % self.list_image_size
        # try:
        image_field = self.get_image_field(obj)
        if image_field:
            thumbnailer = get_thumbnailer(image_field)
            thumbnail = thumbnailer.get_thumbnail({
                'crop': True,
                'size': self.list_image_size.split('x')
            })
            image_url = thumbnail.url
        # except (InvalidImageFormatError, ValueError):
        #     pass

        return '<img src="%s"/>' % image_url

    admin_image.allow_tags = True
    admin_image.short_description = _('Image')


class TranslationMixin(object):
    translation_field = 'title'

    class Media:
        css = {
            'all': (
                'pleiadi_content/languages/stylesheets/admin_languages.css',
            )
        }

    def __fallback_title(self, obj):
        """
        Returns the first localized version of the title, no priority, no logic.

        :param obj:
        :return:
        """
        if getattr(obj, self.translation_field):
            return getattr(obj, self.translation_field)

        admin_language = translation.get_language()
        for language_configuration in Language.available_languages():
            translation.activate(language_configuration.code)

            if getattr(obj, self.translation_field):
                fallback_title = getattr(obj, self.translation_field)
                translation.activate(admin_language)
                return fallback_title

        translation.activate(admin_language)
        return _('Untranslated')

    def admin_title(self, obj):
        """
        Supplying a fallback value for the title field when it's not translated in the user current language.

        @param obj:
        @return:
        """
        fallback_title = getattr(obj, self.translation_field)
        if not fallback_title:
            html = """  <span class="untranslated-message">
                            Untranslated in <span class="language-code">%s</span>
                        </span>
                        [<span class="title-reference">%s</span>]"""

            fallback_title = mark_safe(html.strip()) % (translation.get_language(), self.__fallback_title(obj),)
        return fallback_title

    admin_title.short_description = 'title'
    admin_title.allow_tags = True

    def translations(self, obj):
        """
        If the title is not available for a language the content is considered untranslated in that language.

        :param obj:
        :return:
        """
        titles = []
        app_label = self.model._meta.app_label
        model_name = self.model._meta.model_name

        html = '<div class="flags">%s</div>'

        admin_language = translation.get_language()
        for language_configuration in Language.available_languages():
            translation.activate(language_configuration.code)

            flag_src = static(language_configuration.flag)
            admin_url = reverse("admin:%s_%s_change" % (app_label, model_name,), args=(obj.id,))

            flag_class = ''
            flag_title = _('Already translated: %s')
            if not getattr(obj, self.translation_field):
                flag_class = 'untranslated'
                flag_title = _('To translate%s')

            titles.append(u'<a href="%s" class="flag %s" title="%s"><img src="%s"></a>' % (
                admin_url,
                flag_class,
                flag_title % (getattr(obj, self.translation_field) or '',),
                flag_src
            ))

        translation.activate(admin_language)

        return html % (''.join(titles),)

    translations.allow_tags = True
    translations.short_description = _('Translations')


class FieldsetsMixin(object):
    fieldsets = ()

    def get_fieldsets(self, request, obj=None):
        """
        Sets the fieldsets property for the Admin class, based on the mixins hierarchy

        :param request:
        :param obj:
        :return:
        """
        if not self.fieldsets:
            fieldsets_list = []

            if getattr(self, 'seo_fieldset', None):
                fieldsets_list.append(self.seo_fieldset)
            if getattr(self, 'content_fieldset', None):
                if isinstance(self.content_fieldset, tuple):
                    fieldsets_list.append(self.content_fieldset)
                if isinstance(self.content_fieldset, list):
                    fieldsets_list.extend(self.content_fieldset)
            if getattr(self, 'visibility_fieldset', None):
                fieldsets_list.append(self.visibility_fieldset)
            if getattr(self, 'admin_fieldset', None):
                fieldsets_list.append(self.admin_fieldset)

            if len(fieldsets_list):
                self.fieldsets = tuple(fieldsets_list)

        return super(FieldsetsMixin, self).get_fieldsets(request, obj)


class ReadOnlyFieldsMixin(object):
    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.get_fields() if f.name not in ['id']]

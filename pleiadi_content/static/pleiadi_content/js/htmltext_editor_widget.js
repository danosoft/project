django.jQuery(function(){
    django.jQuery('.htmltext-editor-widget').each(function(){
        CKEDITOR.replace(django.jQuery(this).attr('id'));
    });

    django.jQuery(".htmltext-editor-widget").parents('div.form-row').addClass('htmltext-editor-container')
});
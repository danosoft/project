from django.conf import settings


def rel_alternate(request):
    """
    Sets rel_alternates seo information for templates
    """
    alternate_enabled = getattr(settings, 'SEO_ALTERNATE_ENABLE', False)
    return {
        'rel_alternate': getattr(request, 'rel_alternate', None) if alternate_enabled else None
    }

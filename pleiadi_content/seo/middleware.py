from cms.models import Title
from django.contrib.sites.models import Site
from django.utils import translation
from django.conf import settings
from pleiadi_content.languages import Language

class SeoMiddleware(object):
    """
    Sets rel_alternates seo information in request

    Handles all the cms pages and sets the rel_alternate dict in request.

    Note:
    every Page is related to one Site
    every Page is related to n Title where n is the number of languages the page is available on.

    Title is the "localized" part of a page (page_title, slug, published, ecc...)
    """

    def process_request(self, request):
        alternate = {}
        alternate_enabled = getattr(settings, 'SEO_ALTERNATE_ENABLE', False)

        if alternate_enabled:
            request_language_code = translation.get_language()
            current_page = request.current_page
            current_site = Site.objects.get_current()

            alternates = []
            if current_page:
                # all the language versions of a page (one Title instance for each language)
                current_page_titles = list(Title.objects.public().filter(page=current_page))
                for title in current_page_titles:
                    try:
                        if current_page.site == current_site:
                            current_language = title.language
                            translation.activate(current_language)
                            language_configuration = Language.get_configuration(current_language)

                            if language_configuration.active:

                                lang = language_configuration.rel_alternate_language_code
                                url = 'http://%s%s' % (current_site.domain, current_page.get_absolute_url(language=current_language))
                                alternates.append({
                                    'lang': lang,
                                    'href': url
                                })
                    except Exception, e:
                        print e.message

                alternate = {
                    'source': 'middleware',
                    'items': alternates
                }

            translation.activate(request_language_code)

        setattr(request, 'rel_alternate', alternate)

        return None

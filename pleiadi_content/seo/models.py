from django.db import models
from django.utils import translation
from django.core.urlresolvers import NoReverseMatch
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site
from django.conf import settings
from pleiadi_content.languages import Language


class SeoMixin(models.Model):
    """
    Implementing seo features for all the extending models
    """
    seo_title = models.CharField(_('seo title'), max_length=250, blank=True, null=True)
    seo_description = models.TextField(_('seo description'), blank=True, null=True)

    class Meta:
        abstract = True

    def get_seo_fallback_title(self):
        raise NotImplementedError

    def get_seo_fallback_description(self):
        raise NotImplementedError

    def seo(self):
        """
        Return a dict of seo fields according to the defined fallback.
        """
        return {
            'seo_title': self.seo_title or self.get_seo_fallback_title(),
            'seo_description': self.seo_description or self.get_seo_fallback_description()
        }


class RelAlternateMixin(object):
    """
    Handle the rel_alternate behaviour of the extending models
    """
    def get_absolute_url(self):
        """
        Returns the url of this instance.
        """
        raise NotImplementedError

    def is_visible(self):
        """
        Returns True if the object is visible for current language.
        """
        raise NotImplementedError

    def rel_alternates(self, get_url_method='get_absolute_url', current_app=None):
        """
        returns the rel_alternate dictionary for the instance

        :param get_url_method: method of the instance to use to generate the url
        :current_app
        """
        alternate = None
        alternate_enabled = getattr(settings, 'SEO_ALTERNATE_ENABLE', False)

        if alternate_enabled:
            languages = Language.active_languages()
            current_language_code = translation.get_language()
            current_site = Site.objects.get_current()

            alternates = []
            for language in languages:
                try:
                    translation.activate(language.code)

                    reversed_url = getattr(self, get_url_method)(current_app=current_app)
                    if language.active and reversed_url and self.is_visible():
                        url = 'http://%s%s' % (current_site.domain, reversed_url)
                        alternates.append({
                            'lang': language.rel_alternate_language_code,
                            'href': url
                        })
                except NoReverseMatch:
                    pass

            translation.activate(current_language_code)

            alternate = {
                'source': 'model',
                'items': alternates
            }

        return alternate
